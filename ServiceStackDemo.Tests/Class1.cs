﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Funq;
using NUnit.Framework;

namespace ServiceStackDemo.Tests
{
    [TestFixture]
    public class Class1
    {
        private Container container;

        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            container = new Container();

            container.RegisterAutoWired<HelloService>();
        }

        [Test]
        public void HelloWorldTest()
        {
            var service = container.Resolve<HelloService>();
            var response =  (HelloResponse)service.Any(new Hello {Name = "world"});
            Assert.IsTrue(response.Result.Contains("world"));
        }
    }
}
