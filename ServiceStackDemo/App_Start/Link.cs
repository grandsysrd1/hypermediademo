﻿using ServiceStack.ServiceClient.Web;
using ServiceStack.ServiceHost;

namespace ServiceStackDemo
{
    public class Link
    {
        public static Link Post(string title, IReturn request)
        {
            return new Link(title, "POST", request);
        }

        public static Link Put(string title, IReturn request)
        {
            return new Link(title, "PUT", request);
        }

        public static Link Delete(string title, IReturn request)
        {
            return new Link(title, "DELETE", request);
        }

        private Link(string title, string verb, IReturn request)
        {
            Title = title;
            Verb = verb;
            Href = request.ToUrl(verb);
        }
        public string Title { get; private set; }
        public string Verb { get; private set; }
        public string Href { get; private set; }
    }
}