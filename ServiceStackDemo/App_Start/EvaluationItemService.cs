﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace ServiceStackDemo
{
    [Route("/items", "POST")]
    [Route("/items/{Id}", "GET PUT DELETE")]
    public class EvaluationItem : IReturn<EvaluationItem>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Stamp { get; set; }

        public IEnumerable<Link> Links { get; set; }
    }

    public class EvaluationItemService : Service
    {
        public object Post(EvaluationItem request)
        {
            request.Stamp = DateTime.Now;
            return request;
        }

        public object Get(EvaluationItem request)
        {
            var id = request.Id;
            var name = string.Empty;
            switch (id)
            {
                case 0:
                    name = "henry";
                    break;
                case 1:
                    name = "sheena";
                    break;
            }
            return new EvaluationItem
            {
                Name = name,
                Links = new[] { Link.Put("Update", request), Link.Delete("Delete", request) }
            };
        }

        public object Put(EvaluationItem request)
        {
            request.Name += "+";
            return request;
        }

        public void Delete(EvaluationItem request)
        {

        }
    }
}